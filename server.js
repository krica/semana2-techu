require('dotenv').config();
const express= require ('express');
const body_parser= require('body-parser');
const app = express();
const port = process.env.PORT || 3000;
const URL_BASE = '/tech/v1/';
const usersFile = require('./user.json');
app.listen(port, function(){
console.log('Node esta escuchando: ' + port);
});

app.use(body_parser.json());
//operacion get Colleccion
app.get(URL_BASE+ 'users',
	function(request, response){
	response.send(usersFile);
});
//Petición GET a un unico usuarios mediante ID(instancia
app.get(URL_BASE+'users/:id',
	function(request, response){
	console.log(request.params.id);
	let pos= request.params.id - 1;
	//response.send(usersFile[pos]);
	let respuesta = (usersFile[pos]== undefined) ? {"msg":"usuario no existente"} : usersFile[pos];
  let status_code = (usersFile[pos] == undefined ? 404 : 200)
   response.status(status_code).send(respuesta);
});

app.post(URL_BASE+'users',
function(req,res){
 console.log('POST a users');
 let tam= usersFile.length;
 let new_user = {
   "id": tam + 1 ,
   "first_name" : req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
     "password": req.body.password

 }
 console.log(new_user);
 usersFile.push(new_user);
 res.send("{usuario registrado}");
});
//PUT: para modificación completa
app.put(URL_BASE+'users/:id',
function(req,res){
 console.log('POST a users');
 let pos= req.params.id - 1;

 let edit_user = {
	  "id": pos +1,
   "first_name" : req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
     "password": req.body.password

 }
 console.log(edit_user);
 usersFile[pos]=edit_user;
 res.send({"msg":"Usuario modificado correctamente."});
});
//PATCH: para modificación parcial
app.patch(URL_BASE+'users/:id',
function(req,res){
 console.log('POST a users');
 let pos= req.params.id - 1;

 let edit_user = {
	  "id": pos +1,
   "first_name" : req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
     "password": req.body.password

 }
 console.log(edit_user);
 usersFile[pos]=edit_user;
 res.send({"msg":"Usuario modificado correctamente."});
});




app.delete(URL_BASE+'users/:id',
function(req,res){
 console.log('POST a users');
 let pos= req.params.id - 1;
 usersFile.splice(pos, 1);
 res.send({"msg":"Usuario eliminado correctamente:"+ req.params.id });
});

// Petición GET con Query String (req.query)
app.get(URL_BASE + 'users',
  function(req, res) {
    console.log("GET con query string.");
    console.log(req.query.id);
    console.log(req.query.country);
    res.send(usersFile[pos - 1]);
    respuesta.send({"msg" : "GET con query string"});
});
// LOGIN reto2- users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /tech/v1/login");
    console.log("email:"+request.body.email);
    console.log("password:"+request.body.password);
    let user= usersFile.findIndex(v => v.email == request.body.email  && v.password== request.body.password );
    console.log("Id:"+user);
    if(user!=-1){
      usersFile[user].logged = true;
					//escribir en el  .json
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : usersFile[user].id, "logged" : usersFile[user].logged});

    }else{
      console.log("Login incorrecto.");
      response.send({"msg" : "Login incorrecto."});

    }
   
			  
});
// LOGIN ANTERIOR- users.json
app.post(URL_BASE + 'loginotro',
  function(request, response) {
    console.log("POST /tech/v1/logiotro");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
        us.logged = true;
					//escribir en el  .json
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id, "logged" : us.logged});
        } else {
          console.log("Login incorrecto.");
          response.send({"msg" : "Login incorrecto."});
        }
      }else {
        console.log("Login incorrecto.");
        response.send({"msg" : "Login incorrecto."});
      }
			  }
});
function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   });
  }

	// LOGOUT RETO2- users.json
app.post(URL_BASE + 'logout',
  function(request, response) {
    console.log("POST /tech/v1/logout");
    var userId = request.body.id;
    for(us of usersFile) {
      if(us.id == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
        } else {
          console.log("Logout incorrecto.");
          response.send({"msg" : "Logout incorrecto."});
        }
      } 
      us.logged = true
       
    }
});

//Petición GET RETO2 total de usuarios
  app.get(URL_BASE+'total',
    function(request, response){
      total = usersFile.length;
     response.send({"numeros de usuarios: ": total });
  });



